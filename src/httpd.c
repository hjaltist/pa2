#include <assert.h>
#include <string.h>
#include <netinet/in.h>
#include <glib.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>

//how many active connections we can have
#define MAX_CONNECTIONS 500
//valid request
#define HTTP_OK 200
//unknown request
#define HTTP_NOT_FOUND 404
//the max message size a client can send
#define MESSAGE_SIZE 4096
//gadamn Windows line endings
#define SNL_CRLF "\r\n"
//gadamn Windows line endings
#define DNL_CRLF "\r\n\r\n"
//if we encounter error that means exit
#define EXIT_ERROR -1
//poll timeout to 2 sec
#define POLL_TIMEOUT (2 * 1000)
//socket timeout micro seconds
#define SOCKET_TIMEOUT 30000000
//our server name
#define SERVER_NAME "HelloThereServer/1.0.0"

//keep a hold to our log file
FILE * log_file;
//our array of file descriptors
struct pollfd fds[MAX_CONNECTIONS];
//our array of last active
gint64 time_since_active[MAX_CONNECTIONS];
//client and server
struct sockaddr_in server, client;
//port and main socket
int port, sockfd;
//starts with 1 socket then we increment when new
//ones are added
int number_of_fds = 1;

/////SUPPORTED REQUESTS/////
#define HEAD_REQUEST "HEAD"
#define GET_REQUEST "GET"
#define POST_REQUEST "POST"
///////////////////////////

#define REQUEST_KEY "request_type"
#define HTTP_VERSION_KEY "http_version"
#define METHOD_KEY "requested_method"

/////FUNCTIONS/////
GHashTable * generate_header_table(gchar * header);
GString * html_response_with_data(char * data);
GString * handle_request(char * message, GHashTable * headers, struct sockaddr_in client);
gchar * http_time(char * buffer);
gchar * http_connection_header_response(GHashTable * headers);
void printer(char * message);
void log_to_file(int port, char * address, char * request_method, char * host, int status_code, char * path);
void check_timeouts();
void check_if_connection_should_be_closed(GHashTable * headers, int pos);
void close_connection(int pos);
void stop_blocking(int socket);
void new_connection();
void old_connection(int pos);
void resize_array(int pos);
//////////////////

int main(int argc, char * argv[]) {

	//need two arguments
	if (argc != 2) {
		printer("Port is missing. Please provide a valid port.");
		exit(EXIT_ERROR);
	}

	//try to open our log file, located in our root directory
	if ((log_file = fopen("./logfile.log", "a")) == NULL) {
		printer("Could not open log file.");
		printer("Nothing will be logged.");
	}

	//assign the port number to our port variable
	sscanf(argv[1], "%i", &port);

	//bind the socket and general setup
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0){
    	printer("socket() failed");
    	exit(EXIT_ERROR);
  	}

	//should not block and incoming connections should also not block
	stop_blocking(sockfd);

	//setup the server
	memset(&server, 0, sizeof(server));
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	server.sin_port = htons(port);

	//bind the socket to the server
	if (bind(sockfd, (struct sockaddr *) &server, (socklen_t) sizeof(server)) < 0){
		printer("bind() failed");
		exit(EXIT_ERROR);
	}

	//listen to the port and allow 10 connections
	if(listen(sockfd, MAX_CONNECTIONS) < 0){
		printer("listen() failed");
		exit(EXIT_ERROR);
	}

	//init our poll array
	//http://man7.org/linux/man-pages/man2/poll.2.html
	memset(fds, 0 , sizeof(fds));
	//assign our socket the first slot
	fds[0].fd = sockfd;
  	fds[0].events = POLLIN;

	//loop and accept requests
	while (1) {
		//call poll and wait with a timeout of 5 minutes
		int poll_result = poll(fds, number_of_fds, POLL_TIMEOUT);
		//printer(g_strdup_printf("Got result from poll: %d", poll_result));
		if (poll_result < 0) {
			printer("poll() failed");
			break;
		}
		else if (poll_result == 0){
			//update our available sockets based on inactivity times
			check_timeouts();
			continue;
		}

		//loop and find readable file descriptors that are POLLIN
		for (int pos = 0; pos < number_of_fds; pos++) {
			//not allowed
			if(fds[pos].revents == 0) {
				continue;
			}
			//must be available
			if (fds[pos].revents == POLLIN) {
				//new connection
				if (fds[pos].fd == sockfd) {
					new_connection();
				}
				//new data from already connected
				else {
					old_connection(pos);
					//update the last active time
					time_since_active[pos] = g_get_monotonic_time();
				}
			}
		}
		//update our available sockets based on inactivity times
		check_timeouts();
	}

	//close the socket and exit normamlly
	close(sockfd);
	g_free(&server);
	g_free(&fds);
	return 0;
}

//create a new connection
void new_connection() {
	//to prevent a segmentation fault when we have to many open connections
	if (number_of_fds == MAX_CONNECTIONS) {
		return;
	}

	socklen_t len = (socklen_t) sizeof(client);
	//accept blocks by default so we need to make the socket non blocking
	int connfd = accept(sockfd, (struct sockaddr *) &client, &len);

	if (connfd < 0){
		if (errno == EWOULDBLOCK) {
			printer("All connections up to date");
		} else {
			printer(strerror(errno));
		}
		return;
	}

	printer("Incoming new connection");

	//socket should not block
	stop_blocking(connfd);
	gchar * m = g_strdup_printf("Connection established");
	printer(m);
	g_free(m);

	//add the new connection to our structure
	fds[number_of_fds].fd = connfd;
	fds[number_of_fds].events = POLLIN;
	// Update the time when the connection was last active
	time_since_active[number_of_fds] = g_get_monotonic_time();
	//increment current number of fds
	number_of_fds++;
	//Add connections while possible
	if (connfd != -1) {
		new_connection();
	}
}

void old_connection(int pos) { 
	char message[MESSAGE_SIZE];
	memset(&message, 0, sizeof(message));
	//receive the message from the socket
	size_t size = recv(fds[pos].fd, message, MESSAGE_SIZE, 0);
	if (size <= 0) {
		if (size == 0) {
			printer("Client disconnected");
			close_connection(pos);
			return;
		} 
		if (errno != EWOULDBLOCK) {
			printer("recv() error");
			close_connection(pos);
			return;
		}
	}

	printer("");
	gchar * m = g_strdup_printf("Sending requested data for established connection at position: %d",pos);
	printer(m);
	g_free(m);

	//create a hash table that includes all the headers
	gchar ** head = g_strsplit(message, DNL_CRLF, 2);
	GHashTable * table = generate_header_table(head[0]);
	//handle the request received from the client
	GString * content = handle_request(message, table, client);
	//send our generated response to the server
	size_t success = send(fds[pos].fd, content->str, content->len, 0);

	if (success == (size_t) -1) {
		printer(strerror(errno));
		close_connection(pos);
	} else {
		gchar * mes = g_strdup_printf("Data requested for connection at position: %d successfully sent",pos);
		gchar * data_size = g_strdup_printf("Package size: %d", (int) content->len);
		printer(mes);
		printer(data_size);
		g_free(mes);
		g_free(data_size);

		//check if we should close the connection
		check_if_connection_should_be_closed(table, pos);
	}

	printer("");

	g_strfreev(head);
	g_string_free(content, TRUE);
	g_hash_table_destroy(table);
}

///returns the generated html with our data to be displayed
GString * html_response_with_data(char * data) {
	GString * html = g_string_sized_new(1024);
	g_string_append(html, "<!DOCTYPE html>\n");
    g_string_append(html, "<html>\n");
    g_string_append(html, "<head>\n");
    g_string_append(html, "<title>Assignment 2 Hyper Text Transfer Protocol</title>\n");
	g_string_append(html, "</head>\n");
	g_string_append(html, "<h1 style=\"color:white;\">\n");
	g_string_append(html, "<body style=\"background-color:cornflowerblue;\">\n");
	g_string_append(html, data);
	g_string_append(html, "\n");
	g_string_append(html, "</h1>\n");
	g_string_append(html, "</body>\n");
	g_string_append(html, "</html>\n");
	return html;
}

///handles an incoming http request and creates a response
GString * handle_request(char * message, GHashTable * headers, struct sockaddr_in client) {
	//create our response
	GString * response = g_string_sized_new(1024);
	//it is of type HTTP/1.1.
	g_string_append(response, "HTTP/1.1 ");
	//look up the hash table for the request type
	char * request_type = g_hash_table_lookup(headers, REQUEST_KEY);
	char * request_path = g_hash_table_lookup(headers, METHOD_KEY);
	//check which type of request this is
	int is_get = strcmp(request_type, GET_REQUEST) == 0;
	int is_post = strcmp(request_type, POST_REQUEST) == 0;
	int is_head = strcmp(request_type, HEAD_REQUEST) == 0;

	char buffer[50];
	memset(&buffer, 0, sizeof(buffer));

	gchar * time = http_time(buffer);
	gchar * date = g_strdup_printf("Date: %s\n", time);
	gchar * server = g_strdup_printf("Server: %s\n",SERVER_NAME);
	gchar * connection = http_connection_header_response(headers);

	//these are valid requests
	if (is_get || is_post || is_head) {
		//the message returned inside the html
		GString * displayed_message = g_string_sized_new(1024);
		//the host is located like this -> Host: localhost:1600
		char * request_host = g_hash_table_lookup(headers, "Host");
		//create the response
		gchar * port = g_strdup_printf("%hu", ntohs(client.sin_port));
		gchar * body = g_strdup_printf("http://%s%s %s:%s",request_host,request_path,inet_ntoa(client.sin_addr), port);
		g_string_append(displayed_message, body);

		//if a post request then we add the post paylod to our response
		if (is_post) {
			gchar ** post_data = g_strsplit(message, DNL_CRLF, 2);
			g_string_append(displayed_message, SNL_CRLF);
			g_string_append(displayed_message, post_data[1]);
			g_strfreev(post_data);
		}

		g_string_append(response, "200 OK\n");
		g_string_append(response, "Content-Type: text/html\n");
		g_string_append(response, connection);
		g_string_append(response, date);
		g_string_append(response, server);

		//add the html to the response
		GString * html = html_response_with_data(displayed_message->str);
		//add the length of the html to the content length parameter
		gchar * content_length = g_strdup_printf("Content-Length: %ld\n", html->len);
		g_string_append(response, content_length);
		// If the request is HEAD, skip the payload
		if (!is_head) {
			//finish with and empty line to indicate the end of the headers
			g_string_append(response, "\n");
			//append the html
			g_string_append(response, html->str);
		}
		//log to our file
		log_to_file(ntohs(client.sin_port), inet_ntoa(client.sin_addr), request_type, request_host, 200, request_path);

		g_free(port);
		g_free(body);
		g_free(content_length);
		g_string_free(displayed_message, TRUE);
		//g_free(request_host);
		g_string_free(html, TRUE);
	}
	//otherwise we respond with a 405 method not allowed error
	else {
		g_string_append(response, "405 Method Not Allowed\n");
		g_string_append(response, "Content-Type: text/html\n");
		g_string_append(response, connection);
		g_string_append(response, date);
		g_string_append(response, server);
		//add the html to the response
		GString * html_405 = html_response_with_data("405 Method Not Allowed. Please provide a valid request (GET | POST | HEAD).");
		//add the length of the error html to the content length parameter
		gchar * content_length = g_strdup_printf("Content-Length: %ld\n", html_405->len);
		g_string_append(response, content_length);
		//finish with and empty line to indicate the end of the headers
		g_string_append(response, "\n");
		//append the html error
		g_string_append(response, html_405->str);

		g_free(content_length);
		g_string_free(html_405, TRUE);
	}

	g_free(connection);
	g_free(date);
	g_free(server);

	return response;
}

//create the hash table that holds our headers
GHashTable * generate_header_table(gchar * header) {
	GHashTable * table = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, g_free);
	//split the lines based on new line
	char ** lines = g_strsplit(header, SNL_CRLF, INT_MAX);
	//get the request type e.g GET HTTP/1.1
	char ** request_type = g_strsplit(lines[0], " ", 3);
	g_hash_table_insert(table, g_strdup((gchar *) REQUEST_KEY), g_strdup(request_type[0]));
	g_hash_table_insert(table, g_strdup((gchar *) METHOD_KEY), g_strdup(request_type[1]));
	g_hash_table_insert(table, g_strdup((gchar *) HTTP_VERSION_KEY), g_strdup(request_type[2]));
	//loop through the headers and add to the table
	for (int pos = 1; lines[pos] != NULL; pos++) {
		//split the header based on the :
		char ** key_value = g_strsplit(lines[pos], ": ", 2);
		//if it is null we just continue
		if (key_value[0] == NULL && key_value[1] == NULL) {
			continue;
		}
		//insert the header as a key:value into the hash table
		g_hash_table_insert(table, g_strdup(key_value[0]), g_strdup(key_value[1]));
		//free the array
		g_strfreev(key_value);
	}
	//free the array
	g_strfreev(lines);
	g_strfreev(request_type);

	return table;
}

///the header field Connection
gchar * http_connection_header_response(GHashTable * headers){
	//http 1.0 sent to server defaults to close
	int http_10 = g_strcmp0(g_hash_table_lookup(headers, HTTP_VERSION_KEY), "HTTP/1.0") == 0;
	//something other sent than keep alive defaults to close
	int connection_keep_alive = g_strcmp0(g_hash_table_lookup(headers, "Connection"), "keep-alive") == 0;
	//connection: close sent to server
	int connection_close = g_strcmp0(g_hash_table_lookup(headers, "Connection"), "close") == 0;
	//http 1.0 defaults to connection:close unless it specifies keep alive
	if (http_10){
		if (connection_keep_alive){
			return g_strdup("Connection: keep-alive\nKeep-Alive: timeout=30\n");
		}
		return g_strdup("Connection: close\n");
	}
	//http 1.1 uses Connection:keep-alive unless it specifies close
	if (connection_close){
		return g_strdup("Connection: close\n");
	}

	return g_strdup("Connection: keep-alive\nKeep-Alive: timeout=30\n");
}

//check if we should close the connection after we send the response
void check_if_connection_should_be_closed(GHashTable * headers, int pos) {
	//connection: close sent to server
	int connection_close = g_strcmp0(g_hash_table_lookup(headers, "Connection"), "close") == 0;
	//http 1.0 sent to server defaults to close
	int http_10 = g_strcmp0(g_hash_table_lookup(headers, HTTP_VERSION_KEY), "HTTP/1.0") == 0;
	//something other sent than keep alive defaults to close
	int connection_keep_alive = g_strcmp0(g_hash_table_lookup(headers, "Connection"), "keep-alive") == 0;

	gchar * message_close = g_strdup_printf("Closing connection at %d",pos);

	//if this is http 1.1
	if (!http_10) {
		//if we get a connection close or something else than keep alive
		if (connection_close) {
			printer(message_close);
			close_connection(pos);
		}
		g_free(message_close);
		return;
	}

	if((http_10 && !connection_keep_alive) || (g_hash_table_contains(headers, "Connection") == FALSE)) {
		printer(message_close);
		close_connection(pos);
	}

	g_free(message_close);
}

//loop through active connections and check if we should close them
void check_timeouts() {
	//printer("Checking for timeouts");
	for (int pos = 0; pos < number_of_fds; pos++) {
		if(time_since_active[pos] != 0){
			//printer(g_strdup_printf("Connection %d check", pos));
			//calculate the time since last active
			gint diff = g_get_monotonic_time() - time_since_active[pos];
			//printer(g_strdup_printf("Connection time %d vs %d", diff, SOCKET_TIMEOUT));

			//if it is too old we close the connection
			if (diff >= SOCKET_TIMEOUT) {
				printer(g_strdup_printf("Connection %d timed out.", pos));
				//close the connection
				close_connection(pos);
			}
		}
	}
}

///return the http formatted time
gchar * http_time(char * buffer) {
	time_t now_time = time(0);
	strftime(buffer, sizeof(buffer), "%a, %d %b %Y %H:%M:%S %Z", localtime(&now_time));
	return buffer;
}

///closes the connection and cleans up
void close_connection(int pos) {
	fds[pos].revents = 0;
	close(fds[pos].fd);
	resize_array(pos);
}

///log to the logfile
void log_to_file(int port, char * address, char * request_method, char * host, int status_code, char * path) {
	//we must have an open file
	if (log_file == NULL){
		return;
	}

	//get the current time
	GTimeVal now;
	g_get_current_time(&now);
	now.tv_usec = 0;
	gchar * now_time = g_time_val_to_iso8601(&now);

	//write to the logfile
	fprintf(log_file, "%s : %s:%d %s\n%s%s : %d\n", now_time, address, port, request_method, host, path, status_code);
	fflush(log_file);

	g_free(now_time);
}

///print and newline and flush
void printer(char * message) {
	printf("%s",message);
	printf("\n");
	fflush(stdout);
}

//socket should not block
void stop_blocking(int socket) {
	//get the options and then add the non block flag to
	//the options
	int opts = fcntl(socket,F_GETFL);
	if (opts < 0) {
		printer("Fetching socket options failed");
		exit(-1);
	}

	opts = (opts | O_NONBLOCK);

	if (fcntl(socket,F_SETFL,opts) < 0) {
		printer("Socket unblocking failed");
		exit(-1);
	}
}

void resize_array(int pos) {
	// After a connection is closed, resize the fd array
	for(int i = pos; i < number_of_fds; i++) {
		if (i == number_of_fds - 1) {
			time_since_active[i] = 0;
			continue;
		}

		fds[i].fd = fds[i+1].fd;
		fds[i].events = fds[i+1].events;
		fds[i].revents = fds[i+1].revents;
		time_since_active[i] = time_since_active[i+1];
	}

	number_of_fds--;
}